#!/bin/bash

set -e

if ! [ -f "/etc/ssl/server.key" ]; then
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=local-cas-server" -out "/etc/ssl/server.crt" -keyout "/etc/ssl/server.key"
    cp /etc/ssl/server.crt /cache-certificate/server.crt
fi
