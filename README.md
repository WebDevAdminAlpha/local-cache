To start, run `docker-compose up -d`. You can also specify a different
architecture or a different cache size by using environment variables.
For example `ARCH=arm64 docker-compose up -d` or
`SIZE=500G docker-compose up -d`

Edit `/etc/gitlab-runner/config.toml`, in `runners.docker`:
 - in `volumes` add `"local-cache_server_cert:/cache-certificate"`
 - set `network_mode` to `"local-cache_default"`

Restart gitlab runner.
